class BasketV2 {
  constructor(discounter) {
    this.items = [];
    this.discounter = discounter;
  }

  setDiscounter(discounter) {
    this.discounter = discounter;
  }

  addItem(item) {
    this.items.push(item);
  }

  getCost() {
    const sumPrice = this.items
      .map(item => item.price)
      .reduce((sum, itemPrice) => sum + itemPrice, 0);

    if (this.discounter) {
      return sumPrice - this.discounter.getDiscount(this.items);
    }

    return sumPrice;
  }

  clear() {
    this.items = [];
  }
}

module.exports = { BasketV2 };
