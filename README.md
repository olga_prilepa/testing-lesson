# Материалы с урока по тестированию

[Преза](https://docs.google.com/presentation/d/1rN2YFTRABLeWJ0pz6s0S0IXl2s_nTf0X6UxHharwc1k/edit?usp=sharing)

Файлы:
* basketv1.js - простая реализация корзины покупок
* basketv2.js - реализация с кастомным дискаунтером
* basketv3.js - асинхронный дискаунтер + ошибка
* index.spec.js - тесты
