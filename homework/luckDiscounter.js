class LuckDiscounter {
  getDiscount(items) {
    const sumPrice = items.reduce(
      (sum, item) => sum + item.price * item.count,
      0
    );
    return sumPrice * Math.random();
  }
}

module.exports = { LuckDiscounter };
