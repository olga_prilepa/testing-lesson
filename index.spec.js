const { BasketV1 } = require("./basketv1");
const { BasketV2 } = require("./basketv2");
const { BasketV3 } = require("./basketv3");

describe("BasketV1", () => {
  describe("getCost", () => {
    it("должен возращать цену с учетом скидки 10% если пользователь набрал товаров на сумму более 3000 рублей", () => {
      const backet = new BasketV1();

      backet.addItem({ name: "товар 1", price: 500 });
      backet.addItem({ name: "товар 2", price: 1500 });
      backet.addItem({ name: "товар 3", price: 1000 });
      backet.addItem({ name: "жевачка", price: 1 });

      const cost = backet.getCost();
      expect(cost).toEqual(3001 * 0.9);
    });
  });
});

describe("BasketV2", () => {
  describe("getCost", () => {
    let backet;
    beforeEach(() => {
      backet = new BasketV2();
    });
    afterEach(() => {
      backet.clear();
    });
    beforeAll(() => {});
    afterAll(() => {});
    it("должен вернуть цену без скидки, если не передан discounter", () => {
      backet.addItem({ name: "товар", price: 3001 });

      expect(backet.getCost()).toEqual(3001);
    });

    it("должен вернуть цену с учетом скидки discounter", () => {
      const spy = jest.fn(() => 200);
      const discounter = {
        getDiscount: spy
      };

      backet.setDiscounter(discounter);

      backet.addItem({ name: "товар", price: 3001 });

      expect(backet.getCost()).toEqual(2801);
      expect(spy).toHaveBeenCalledWith([{ name: "товар", price: 3001 }]);
    });
  });
});

describe("BasketV3", () => {
  describe("getCost", () => {
    it("должен выбросить исключение если discounter не предоставлен", async () => {
      const backet = new BasketV3();

      backet.addItem({ name: "Товар", price: 3001 });

      return expect(backet.getCost()).rejects.toThrow("provide discounter!");
    });

    it("должен применить скидку из discounter если он предоставлен", async () => {
      const discounter = {
        getDiscount: jest.fn(() => Promise.resolve(301))
      };

      const backet = new BasketV3(discounter);

      backet.addItem({ name: "Товар", price: 3001 });

      expect(await backet.getCost()).toEqual(2700);
    });
  });
});
